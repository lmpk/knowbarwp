<?php
/**
 * Plugin Name: knowbarwp
 * Description: Let's learn something new
 * Author: Dawid Mohr
 * Version 0.01
 */

// require_once("autoload.php");
// $t = new \theCodingCompany\test();

if(!defined('ABSPATH')){
    exit;
}


// Load Scripts
// require_once(plugin_dir_path(__FILE__).'/includes/youtubesubs-scripts.php');

// Load Class
require_once(plugin_dir_path(__FILE__).'/includes/widged-mastodon-class.php');

// Register Widget
function register_mastodon(){
    register_widget('Mastodon_Widget');
  }

// Hook in func
add_action('widgets_init', 'register_mastodon');

// add_action("admin_menu", "addMenu");

// function addMenu() {
//     add_menu_page("Example Options", "Mastodon plugin", 4, "example-options", "exampleMenu");
//     add_submenu_page("example-options", "Mastodon admin", "Mastodon admin", 4, "Example-option-1", "option1" );
// }

// function exampleMenu()
// {
//     echo "<h1>Coming Soon!</h1>";
// }

// function option1() {
//     echo "Opcja";
// }
